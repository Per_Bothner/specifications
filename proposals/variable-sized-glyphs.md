# Variable-sized glyphs and local line-breaking

## Summary

The traditional terminal models a rectangular grid of fixed-sized
character cells, with characters limited to single-width or double-width.
This generalizes the model to support variable-width cells (renamed _glyphs_)
along with variable-height lines.
We also proposes a _glyph mode_ to take better advantage of this model.

While some applications may need precise control over glyph placement
(and thus need font matrics), we will focus on use cases that neither
want or need such control.  Instead, we assume line-breaking (dividing
logical lines that are wider than the window) is handled by the terminal,
and the protocol is at the level of logical lines.

## Motivation

For some (human) languages using a single-width font (or even duo-width
with double-wide characters) is awkward and/or considered very ugly.
Even for English it's considered ugly - you're probably reading this
using a variable-width font!

Once you're dealing with variable-sized characters
dealing with images and graphics is more natural, as they can
be adressed as extra-large characters.

If the communication between terminal and application is at
the glyph level (using "glyph mode") then some simplifications are
possible.  The terminal and application don't have to agree on
which characters are double-width, as that is now the responsability of the
terminal.

Line-breaking is also the responsibility of the
terminal, so the application doesn't have to redraw if the screen
width changes.  (Having the application redraw is inherently fragile,
since it cannot know the width the terminal will have when the re-display
is done.)

If multiple people are collaborating using a shared REPL,
they may each have different screen widths,
which also argues for terminal-local line-breaking.

## Concepts

The phrase _to-do_ means something that needs to be decided
to make this a useful specification.
The word _extension_ refers to possible future follow-up specifications.

For example, the following will assume left-to-right text,
but an _extension_ should deal with right-to-left and bi-directional text.

We generalize "character" to "glyph", which includes
characters, images, spacing, and markers (discussed below).
Each glyph is displayed in a rectangular area of the screen,
though the area may have zero width or height.

A terminal may be in the traditional _column mode_ or the new _glyph  mode_,
depending on some (_to-do_) escape sequence.
In glyph mode cursor adressing is by glyph, rather than by column.

_To-do:_ Should glyph mode automatically implies insert mode?
(There doesn't seem much use for overrwite mode directly,
but it might simplify enhancing existing libraries
to take advantage of glyph mode.)

Each glyph has a non-negative width in pixels,
and also a non-negative width in _columns_.
There is no fixed ratio between these:
The width in columns is merely the repeat count needed
when in column mode for a Cursor Right escape sequence to
move from before to after the glyph.
(In glyph mode the needed repeat count is always one,
which enables the application to work without knowing column width.)

## Kinds of glyphs

A regular character is traditionally one or two columns,
depending on whether it is single-width or double-width.
There may also be zero-width characters and possibly wider characters.

A _spacer_ corresponds to some amount of "white space"
(though an _extension_ could allow dots or some other kind of fill character).
Number of columns typically corresponds to the width of the space.
Spacers should be used for tab characters,
as well as for the spacing between an input line and a right prompt
(as in `fish`).
Can also be used for indentation and centering.
Corresponds to TeX's _glue_ concept.

_Optional-newlines_ are spacing that may cause following glyphs to
be displayed on a fresh line if there isn't enough space on the current line.
These are like spacer glyphs that are logically like a space (in terms of
cursor adressing), but the terminal may use them to (soft-)break lines.

An image or other compound graphics.

_Begin-group_ and _end-group_ glyphs indicate logical grouping:
These are normally zero-width and zero columns.
The glyphs in between a _begin-group_ and the matching _end-group_
logically or visually belong together.
A begin-group corresponds to an HTML begin-element,
while an end-group corresponds to an HTML end-element.

A line-end marker isn't really a glyph, but acts like one for adressing.

## Glyph mode behavior

In glyph mode, certain escape sequences have different behavior,
and some are disallowed or undefined.

Some details _to-do_, but note that the primary goal is that
an application such as an input editor for a REPL (for example GNU `readline`)
should work if using a variable-width font, without having to know the width
of any character, or the current screen width (in characters or pixels).
A secondary goal is that this should be possible with relatively
modest changes to existing input editors.

Cursor Left and Cursor Right move the specified number of
glyphs forwards or backwards, rather than a specied number of columns.
Specifically, double-width characters are a single glyph.
A line marker is counted as a glyph.
While an optional-newline counts as one glyph,
traditional line-wrap (as might be in the middle of a word)
is zero glyphs - the cursor may be displayed at the end of the
previous line or the beginning of the next.
(_To-do_ specify or at least recommend which to use when.)

Delete character likewise deletes a specied number of glyphs, not columns.
_To-do_: If a cursor is positioned before a begin-group glyph and it is
deleted, there are two plausible behaviors: Remove the begin-group
*and* the corresponding end-group glyph; or delete the
entire group (delete begin-group, end-group, and all glyphs between).

Cursor Up and Cursor Down.

Home position.

## Columns mode with glyphs

The presence of glyphs in the data model requires some
extensions even when in traditional column-mode.

In column mode, an application should move the cursor enough columns
so it does not end in the "middle" of a glyph.  It is allowed for
the cursor to be temporarily in the middle of a glyph,
but the terminal is not required to show a visual indication of that.
An application should not overwrite or replace parts of a glyph (such moving
partway into the glyph and writing a regular single-column character);
if it does the terminal should first replace the glyph by single-width
characters repeated the column width of the glyph: Spaces should be
used for spacing glyphs, and some other simple character (such as `?`)
should be used for other glyphs.
